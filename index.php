<?php header('Access-Control-Allow-Origin: *'); ?>


<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	

	
  <style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 450px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      padding-top: 20px;
      background-color: #f1f1f1;
      height: 100%;
    }
    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 15px;
    }
    
    /* On small screens, set height to 'auto' for sidenav and grid */
    @media screen and (max-width: 767px) {
      .sidenav {
        height: auto;
        padding: 15px;
      }
      .row.content {height:auto;} 
    }
  </style>
  

 
<!-- Javascript -->


</head>
<body>

<div id="navbar">
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#">Logo</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">Home</a></li>
        <li><a href="#">About</a></li>
        <li><a href="#">Projects</a></li>
        <li><a href="#">Contact</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
      </ul>
	     <form action="http://localhost:8080/user/get/" id="search" role="form" method="GET">
    <div class="form-group">
        <label for="name" class="col-sm-2 control-label">Name</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="name" name="name" placeholder="search house" value="">
        </div>
    </div>
 
    <div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
            <input id="submit" name="submit" type="submit" value="Send" class="btn btn-primary">
        </div>
    </div>
  
</form>
    </div>
	<div id="search-result">
	</div>
  </div>
</nav>
</div>
  
<div class="container-fluid text-center">    
  <div class="row content">
    <div class="col-sm-2 sidenav">
      <p><a href="#">Link</a></p>
      <p><a href="#">Link</a></p>
      <p><a href="#">Link</a></p>
    </div>
    <div class="col-sm-8 text-left"> 
     <div id = "stage" style = "background-color:#cc0;">   
      </div>
	    <div class="col-sm-8 text-left"> 
	<div id="persons">
	
	</div>
	</div>
	  	
     
    <div class="col-sm-2 sidenav">
      <div class="well">
        <p>ADS</p>
      </div>
      <div class="well">
        <p>ADS</p>
      </div>
    </div>
  </div>
</div>

<footer class="container-fluid text-center">
  <p>Footer Text</p>
</footer>

<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>

<script>
$.ajax({
        url:"http://localhost:8080/houses/get-all",
        type: "GET",
        dataType: 'json', 
        crossDomain: "true",      
        success: function (result) {
            if (result.type == false) {
                alert("Error occured:" + result.data);
                return false;
            }
			console.log(result.data)
            $.each(result,function(index,obj){
 
              $("#persons").append("  <div class='col-m-9'><span class='name'><tr><div><td>Nome: <span>"  + obj.name + "</span></td></div> " 	
			  +"<div><span><td> Descrição: " +obj.description+"</td></span></td><br>"
			  +"<div><span><td> Preço: " +obj.price+"</td></span></td>"
			  +"<div><span><td> Localização: " +obj.localization+"</td></span></td>"
		      +"</div></tr></span></div><br>");
	   		  
      });
	  }
	  });
</script>



<script type="text/javascript">


 $(function () {
	
	
	 
        function search() {
            
			//var query = $("input:text").val();
               var query=$("#search").find("input[name=name]").val();
			
			$.get("http://localhost:8080/get/?id=" + encodeURIComponent(query), function (data) {
    if (!data) return false;
      $("#search-result").empty();                            
      $("#search-result").append("<tr><td class='movie'>" + data.id + "</td><td>" + data.name + "</td><td>" + data.sobrename + "</td></tr>");
                  
})
		return true;
        }
 
        
		  
		  
		  $("#search").submit(function(e) {
		  search();
    e.preventDefault();
});

		
    })
	
	
	
	
	
</script>

	
	 
</body>
</html>
